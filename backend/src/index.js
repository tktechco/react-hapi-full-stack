const createClient = require("redis").createClient;
let client = createClient({
    port: 6379, // replace with your port
    host: "localhost"
});

export default client;