"use strict";
// const Redis = require('ioredis');
//only one connection, starts when this file run
// const redis = new Redis();
const Hapi = require("@hapi/hapi");
const Path = require("path");
const Inert = require("inert");

//define server variables
const server = Hapi.server({
  port: 3000,
  host: "localhost",
  routes: {
    files: {
      relativeTo: Path.join(__dirname, "dist")
    }
  }
});

//define function for server routes
const init = async () => {
  await server.register(Inert);

  //global options?
  server.route({
    method: "GET",
    path: "/{param*}",
    handler: {
      directory: {
        path: ".",
        redirectToSlash: true,
        index: true
      }
    }
  });


  server.route({
    method: "GET",
    path: "/bundle.js",
    handler: {
      file: {
        path: "bundle.js",
        filename: "client.js", // override the filename in the Content-Disposition header
        mode: "attachment", // specify the Content-Disposition is an attachment
        lookupCompressed: true // allow looking for script.js.gz if the request allows it
      }
    }
  });

  server.route({
    method: "GET",
    path: "/test",
    handler: (request, h) => {

      // request.log(['a', 'name'], "Request name");
      // or
      request.logger.info('In handler %s', request.path);

      return 'Hello, ${encodeURIComponent(request.params.name)}!';
    }
  });

  server.route({
    method: "GET",
    path: "/test2",
    handler: (request, h) => {
      redis.set('foo', 'bar');
      return redis.get('foo');
    }
  });

  await server.register({
    plugin: require('hapi-pino'),
    options: {
      prettyPrint: true,
      logEvents: ['response', 'onPostStart']
    }
  });

  await server.start();
  console.log(`Server running at: ${server.info.uri}`);

};


process.on('unhandledRejection', (err) => {
  console.log(err);
  process.exit(1);
});

init();
