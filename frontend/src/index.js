import React from "react";
import ReactDOM from "react-dom";
import { App } from "./components/App.jsx";
//when developing this isnt needed as css is inline
//import "./bundle.css";

ReactDOM.render(<App />, document.getElementById("root"));